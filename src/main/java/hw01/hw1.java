package hw01;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class hw1 {

    static int[] reverseArray(int[] arr) {
        Arrays.sort(arr);

        int[] newArr = new int[arr.length];

        for (int i = 0; i < arr.length; i++) {
          newArr[i] = arr[arr.length-i-1];
        }

        return newArr;
    }

    static void printIntsFromArr(int[] arr) {
        System.out.println("");
        int[] reversedArray = reverseArray(arr);
        for (int number: reversedArray) {
            System.out.print(number + ", ");
        }
    }

    static int[] addingIntToUserInts(int[] userIntArr, int number) {
        int[] newArr = new int[ userIntArr.length + 1 ];

        for (int i = 0; i < userIntArr.length; i++) {
            newArr[i] = userIntArr[i];
        }

        newArr[userIntArr.length] = number;
        return newArr;
    }

    static int[] compareInts(int a, int b, int[] arrInts) {
        while (a != b) {

            if ( a < b ) {
                arrInts = addingIntToUserInts(arrInts, b);
                b = enteringIntByUser("Your number is too big. Please, try again.");
            }
            if ( a > b ) {
                arrInts = addingIntToUserInts(arrInts, b);
                b = enteringIntByUser("Your number is too small. Please, try again.");
            }
        }
        return arrInts;
    }

    static int getRandomInt() {
        int min = 0;
        int max = 100;
        int diff = max - min;

        Random rnd = new Random();
        int rndInt = rnd.nextInt(diff+1);

        return rndInt;
    }

    static int enteringIntByUser(String message) {
        System.out.println(message);
        Scanner scanner = new Scanner(System.in);
        return scanner.nextInt();
    }

    static String enteringNameByUser(String message) {
        System.out.println(message);
        Scanner scanner = new Scanner(System.in);
        return scanner.nextLine();
    }


    public static void main(String[] args) {
        int rndInt = getRandomInt();
        int userInt;
        String userName;

        System.out.println("Let the game begin!");
        userName = enteringNameByUser("Enter your name");
        userInt = enteringIntByUser("Enter the number i'm thinking about");

        int[] userInts = {userInt};
        userInts = compareInts(rndInt, userInt, userInts);

        System.out.printf("Congratulations, %s!", userName);
        printIntsFromArr(userInts);
    }
}

package hw08;

public class Fish extends Pet {
    private Species species;
    private String nickname;
    private int age;

    @Override
    public Species getSpecies() {
        return species;
    }

    @Override
    public void setSpecies(Species species) {
        this.species = species;
    }

    @Override
    public String getNickname() {
        return nickname;
    }

    @Override
    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    @Override
    public int getAge() {
        return age;
    }

    @Override
    public void setAge(int age) {
        this.age = age;
    }

    public Fish(Species species, String nickname, Species species1, String nickname1, int age) {
        super();
        this.species = species1;
        this.nickname = nickname1;
        this.age = age;
    }

    public Fish(Species species, String nickname, int age, int trickLevel, String[] habits, Species species1, String nickname1, int age1) {
        super(species, nickname, age, trickLevel, habits);
        this.species = species1;
        this.nickname = nickname1;
        this.age = age1;
    }

    public Fish(Species species, String nickname, int age) {
        super();
        this.species = species;
        this.nickname = nickname;
        this.age = age;
    }

    public Fish(Species species, String nickname) {
        super();
    }

    public Fish(Species species, String nickname, int age, int trickLevel, String[] habits) {
        super(species, nickname, age, trickLevel, habits);
    }

    public Fish() {
        super();
    }

    void respond()  {
        System.out.println("Hi, im a fish");
    }
}

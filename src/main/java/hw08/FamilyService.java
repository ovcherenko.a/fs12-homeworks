package hw08;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Random;
import java.util.stream.Collectors;

public class FamilyService {

    CollectionFamilyDao collectionFamilyDao = new CollectionFamilyDao();

    // OK
    public ArrayList<Family> getAllFamilies() {
        // получить список всех семей.
        return collectionFamilyDao.getAllFamilies();
    }

    // OK
    public Family getFamilyById(int index) {
        // принимает индекс семьи, возвращает `Family` по
        // указанному индексу.
        return collectionFamilyDao.getFamilyByIndex(index);
    };

    // OK
    public void displayAllFamilies() {
        // вывести на экран все семьи (в индексированном списке)
        // со всеми членами семьи
        collectionFamilyDao.getAllFamilies()
                .stream()
                .forEach(x->System.out.println(x.toString()));
    }

    // OK
    public ArrayList<Family> getFamiliesBiggerThan(int index) {
        // найти семьи с количеством людей больше чем
        // (принимает количество человек и возвращает все семьи
        // где количество людей больше чем указанное);
        // выводит информацию на экран.

        return collectionFamilyDao.getAllFamilies().stream()
                .filter(x -> x.countFamily() > index)
                .peek(System.out::println)
                .collect(Collectors.toCollection(ArrayList::new));
    };

    // OK
    public ArrayList<Family> getFamiliesLessThan(int index) {
        // найти семьи с количеством людей меньше чем
        // (принимает количество человек и возвращает
        // все семьи где количество людей меньше чем указанное);
        // выводит информацию на экран.

        return collectionFamilyDao.getAllFamilies().stream()
                .filter(x -> x.countFamily() < index)
                .peek(System.out::println)
                .collect(Collectors.toCollection(ArrayList::new));
    }

    // OK
    public int countFamiliesWithMemberNumber(int number) {
        // подсчитать число семей с количеством людей равное
        // переданному числу.

        return (int) collectionFamilyDao.getAllFamilies()
                .stream()
                .filter(x-> x.countFamily() == number)
                .count();
    }

//    public deleteAllChildrenOlderThan(int age) {
//
//    }

    // OK
    public void createNewFamily(Human men, Human woman) {
        // создать новую семью (принимает 2 параметра типа Human)
        // - создает новую семью, сохраняет в БД.
        collectionFamilyDao.saveFamily(new Family(woman, men));
    }

    // OK
    public void deleteFamilyByIndex(int index) {
        // удалить семью по индексу в списке - удаляет семью из БД.
        collectionFamilyDao.deleteFamily(index);
    }

    //OK
    public Family bornChild(Family family, String menName, String womanName) {
        // родить семьей ребенка (принимает Family и 2 типа String:
        // мужское и женское имя) - в данной семье появляется
        // новый ребенок с учетом данных родителей, информация
        // о семье обновляется в БД; метод возвращает обновленную
        // семью. Если рожденный ребенок мальчик -
        // ему присваивается мужское имя, если девочка - женское.
        Random random = new Random();
        Date d = new Date();

        Boolean sex = random.nextBoolean();
        Human bornChild;
        if(sex) {
            bornChild = new Human(menName, family.getFather().getSurname(), d.getYear());
        } else {
            bornChild = new Human(womanName, family.getFather().getSurname(), d.getYear());
        }

        family.addChild(bornChild);
        collectionFamilyDao.replaceFamily(family);
        return family;
    }

    // OK
    public Family adoptChild(Family family, Human child) {
        // усыновить ребенка (принимает 2 параметра: Family, Human)-
        // в данной семье сохраняется данный ребенок, информация
        // о семье обновляется в БД; метод возвращает обновленную семью.
        family.addChild(child);
        collectionFamilyDao.replaceFamily(family);
        return family;
    }

    // OK
    public void deleteAllChildrenOlderThen(int age) {
        // удалить детей старше чем (принимает int) - во всех
        // семьях удаляются дети, которые старше указанно возраста,
        // информация обновляется в БД.

        ArrayList<Family> family = collectionFamilyDao.getAllFamilies();

        for (Family f: family) {
            for (Human child: f.getChildren()) {
                if(child.getYears() > age) {
                    f.deleteChild(child);
                }
            }
        }
        collectionFamilyDao.replaceAllFamilies(family);
    }

    // OK
    public int count() {
        // возвращает количество семей в БД.
        return collectionFamilyDao.getAllFamilies().size();
    }

    // OK
    public HashSet<Pet> getPets(int familyIndex) {
        // принимает индекс семьи, возвращает список домашних
        // животных, которые живут в семье.
        return collectionFamilyDao.getFamilyByIndex(familyIndex).getPet();
    }

    // OK
    public void addPet (int familyIndex, Pet pet) {
        // принимает индекс семьи и параметр Pet - добавляет
        // нового питомца в семью, обновляет данные в БД.
        collectionFamilyDao.getFamilyByIndex(familyIndex).setPet(pet);
    }

}

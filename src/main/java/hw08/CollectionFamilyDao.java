package hw08;

import java.util.*;

public class CollectionFamilyDao implements FamilyDao{

    List<Family> AllFamilies = new ArrayList<>();

    @Override
    public ArrayList<Family> getAllFamilies() {
        return (ArrayList<Family>) AllFamilies;
    }

    @Override
    public Family getFamilyByIndex(int index) {
        return AllFamilies.get(index);
    }

    @Override
    public Boolean deleteFamily(int index) {
        Family f = AllFamilies.remove(index);
        if(f instanceof Family) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public Boolean deleteFamily(Family family) {
        return AllFamilies.remove(family);
    }

    @Override
    public void saveFamily(Family family) {
        AllFamilies.add(family);
    }

    public void replaceFamily(Family family) {
        AllFamilies.set(AllFamilies.indexOf(family), family);
    }

    public void replaceAllFamilies(ArrayList<Family> families) {
        AllFamilies = families;
    }

}

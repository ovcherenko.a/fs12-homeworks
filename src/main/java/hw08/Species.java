package hw08;

public enum Species {
    UNKNOWN,
    CAT,
    DOG,
    BIRD,
    RABBIT,
    FISH,
    GUINEA_PIG,
    DOMESTICCAT,
    ROBOCAT
}

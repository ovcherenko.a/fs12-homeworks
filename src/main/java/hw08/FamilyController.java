package hw08;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Random;

public class FamilyController {
    FamilyService familyService = new FamilyService();

    public ArrayList<Family> getAllFamilies() {
        return familyService.getAllFamilies();
    }

    public Family getFamilyById(int index) {
        return familyService.getFamilyById(index);
    };

    public void displayAllFamilies() {
        familyService.displayAllFamilies();

    }

    public ArrayList<Family> getFamiliesBiggerThan(int index) {
        return familyService.getFamiliesBiggerThan(index);
    };

    public ArrayList<Family> getFamiliesLessThan(int index) {
        return familyService.getFamiliesLessThan(index);
    }

    public int countFamiliesWithMemberNumber(int number) {
        return familyService.countFamiliesWithMemberNumber(number);
    }

    public void createNewFamily(Human men, Human woman) {
        familyService.createNewFamily(men, woman);
    }

    public void deleteFamilyByIndex(int index) {
        familyService.deleteFamilyByIndex(index);
    }

    public Family bornChild(Family family, String menName, String womanName) {
        return familyService.bornChild(family, menName, womanName);
    }

    public Family adoptChild(Family family, Human child) {
        return familyService.adoptChild(family, child);
    }

    public void deleteAllChildrenOlderThen(int age) {
        familyService.deleteAllChildrenOlderThen(age);
    }

    public int count() {
        return familyService.count();
    }

    public HashSet<Pet> getPets(int familyIndex) {
        return familyService.getPets(familyIndex);
    }

    public void addPet (int familyIndex, Pet pet) {
        familyService.addPet(familyIndex, pet);
    }

}

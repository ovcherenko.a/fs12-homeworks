package hw08;

import java.util.HashSet;

public class Dog extends Pet implements PetInterface {
//    private Species species;
    private String nickname;
    private int age;

    public Dog(String nickname) {
        super();
        this.nickname = nickname;
    }

    @Override
    public Species getSpecies() {
        return species;
    }

    @Override
    public void setSpecies(Species species) {
        this.species = species;
    }

    @Override
    public String getNickname() {
        return nickname;
    }

    @Override
    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    @Override
    public int getAge() {
        return age;
    }

    @Override
    public void setAge(int age) {
        this.age = age;
    }

    public Dog(String nickname, int age) {
        super();
        this.nickname = nickname;
        this.age = age;
    }

    public Dog(String nickname, int age, int trickLevel, HashSet<String> habits) {
        super(nickname, age, trickLevel, habits);
    }

    public Dog(Species species, String nickname, int age) {
        super();
        this.species = species;
        this.nickname = nickname;
        this.age = age;
    }

    public Dog(Species species, String nickname, int age, int trickLevel, String[] habits, String nickname1) {
        super(species, nickname, age, trickLevel, habits);
        this.nickname = nickname1;
    }

    public void foul() {
        System.out.println("Нужно хорошо замести следы...");
    }

    @Override
    public String toString() {
        return "Dog{" +
                "nickname='" + nickname + '\'' +
                ", age=" + age +
                ", trickLevel=" + getTrickLevel() +
                ", habits=" + this.getHabits() +
                '}';
    }

    @Override
    void respond() {
        System.out.printf("Привет, хозяин. Я - твой пёс %s. Я соскучился!%n", nickname);
    }
}

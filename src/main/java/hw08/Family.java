package hw08;

import javax.swing.plaf.basic.BasicComboBoxUI;
import java.util.*;

public class Family {

    private Human mother;
    private Human father;
    private ArrayList<Human> children = new ArrayList<>();
    private HashSet<Pet> pet = new HashSet<>();
    private Family family;

    public void addChild(Human child) {
        this.children.add(child);
    }

    public void deleteChild(Human child) {
        children.remove(child);
    }

    public boolean deleteChild(int childIndex) {
        Human x = children.remove(childIndex);
        if(x instanceof Human) {return true;}
        return false;
    }

    public int countFamily() {
        return 2 + children.size();
    }

    public Human getMother() {
        return mother;
    }

    public Human getFather() {
        return father;
    }

    public ArrayList<Human> getChildren() {
        return children;
    }

    public HashSet<Pet> getPet() {
        return pet;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public void setChildren(ArrayList<Human> children) {
        this.children = children;
    }

    public void setPet(Pet pet) {
         this.pet.add(pet);
    }

    public Family(Human mother, Human father) {
        this.mother = mother;
        this.father = father;
        this.family = this;
    }

    public Family() {
    }

    @Override
    public String toString() {
        return "Family{" +
                "mother=" + mother +
                ", father=" + father +
                ", children=" + children.toString() +
                ", pet=" + this.pet.toString() +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Family family1 = (Family) o;
        return Objects.equals(mother, family1.mother) &&
                Objects.equals(father, family1.father) &&
                children.equals(family1.children) &&
                Objects.equals(this.pet, family1.pet) &&
                Objects.equals(family, family1.family);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(mother, father, pet, family);
        result = 31 * result + children.hashCode();
        return result;
    }

    protected void finalize() {
        this.toString();
    }
}

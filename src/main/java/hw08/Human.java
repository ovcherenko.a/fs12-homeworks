package hw08;

import java.time.Instant;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.*;

public class Human {
    private String name;
    private String surname;
    private long birthDate;
    private int iq; // (целое число от 0 до 100)
    private Pet pet;
    private HashMap<DayOfWeek, String> schedule = new HashMap<>();   // Расписание внерабочих занятий [день недели][тип секции \ отдыха]
    private Family family;

    public String birthDateToString() {
        LocalDate date = Instant.ofEpochMilli(this.birthDate).atZone(ZoneId.systemDefault()).toLocalDate();
        return  date.getDayOfMonth() + "/" + date.getMonthValue() + "/" + date.getYear();
    }

    public String describeAge(){
        LocalDate date = Instant.ofEpochMilli(this.birthDate).atZone(ZoneId.systemDefault()).toLocalDate();
        LocalDate today = LocalDate.now();
        long deltaYear = this.getYears();
        int deltaMonth = today.getMonthValue() - date.getMonthValue();
        int deltaDay = today.getDayOfMonth() - date.getMonthValue();
        return deltaYear + " years " + deltaMonth + " months " + deltaDay + " ";
    }

    public long getYears() {
        long ageInMillis = new Date().getTime();
        return ageInMillis - birthDate;
    }

    void greetPet() {
        System.out.printf("Привет, %s%n",pet.getNickname());
    }

    void describePet() {
        String trickLevel = pet.getTrickLevel() > 50 ? "очень хитрый" : "почти не хитрый";
        System.out.printf("У меня есть %s, ему %d лет, он %s",
                pet.getSpecies(), pet.getAge(), trickLevel);
    }

    public Human(String name, String surname, int birthDate) {
        this.name = name;
        this.surname = surname;
        this.birthDate = birthDate;
    }

    public Human(String name, String surname, int birthDate, Human mother, Human father) {
        this.name = name;
        this.surname = surname;
        this.birthDate = birthDate;
    }

    public Human(String name, String surname, int birthDate, int iq, Pet pet, Human mother, Human father, HashMap<DayOfWeek, String> schedule) {
        this.name = name;
        this.surname = surname;
        this.birthDate = birthDate;
        if(iq >= 0 && iq <= 100) {
            this.iq = iq;
        }
        this.pet = pet;
        this.schedule = new HashMap<>();
    }



    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

//    public long getYears() {
//        return years;
//    }
//
//    public long getbirthDate() { return birthDate;}

    public int getIq() {
        return iq;
    }

    public Pet getPet() {
        return pet;
    }

    public HashMap<DayOfWeek, String> getSchedule() {
        return schedule;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setBirthDate(int birthDate) {

        this.birthDate = birthDate;
    }

    public void setIq(int iq) {
        this.iq = iq;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    public void setSchedule(HashMap<DayOfWeek, String> schedule) {
        this.schedule = schedule;
    }

    public Family getFamily() {
        return family;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    public Human() {
    }

    @Override
    public String toString() {
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("Europe/Kiev"));
        Date d = new Date(birthDate);
        cal.setTime(d);
        return "Human{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", birthDate=" + cal.get(Calendar.DATE) +
                "/" + cal.get(Calendar.MONTH) +
                "/" + cal.get(Calendar.YEAR) +
                ", iq=" + iq +
                ", schedule=" + schedule.toString() +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return birthDate == human.birthDate &&
                iq == human.iq &&
                Objects.equals(name, human.name) &&
                Objects.equals(surname, human.surname) &&
                Objects.equals(pet, human.pet) &&
                schedule.equals(human.schedule) &&
                Objects.equals(family, human.family);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(name, surname, birthDate, iq, pet, family);
        result = 31 * result + schedule.hashCode();
        return result;
    }

    protected void finalize() {
        this.toString();
    }
}

package hw08;

import java.util.ArrayList;

public interface FamilyDao {

    ArrayList<Family> getAllFamilies();

    Family getFamilyByIndex(int index);

    Boolean deleteFamily(int index);

    Boolean deleteFamily(Family family);

    void saveFamily(Family family);

}

package hw08;

public class RoboCat extends Pet{
    private Species species;
    private String nickname;

    public RoboCat(Species species, String nickname, Species species1, String nickname1) {
        super();
        this.species = species;
        this.nickname = nickname;
    }

    @Override
    public Species getSpecies() {
        return species;
    }

    @Override
    public void setSpecies(Species species) {
        this.species = species;
    }

    public RoboCat(Species species, String nickname, int age, int trickLevel, String[] habits, Species species1, String nickname1) {
        super(species, nickname, age, trickLevel, habits);
        this.species = species1;
        this.nickname = nickname1;
    }

    public RoboCat(Species species, String nickname) {
        super();
        this.species = species;
        this.nickname = nickname;
    }

    void foul() {
        System.out.println("Нужно хорошо замести следы...");
    }

    @Override
    public String getNickname() {
        return nickname;
    }

    @Override
    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    @Override
    void respond() {
        System.out.printf("Привет, хозяин. Я - робот %s. Я соскучился!%n", nickname);
    }
}

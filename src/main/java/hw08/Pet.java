package hw08;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Objects;

public abstract class Pet {
//    private Species species;
    private String nickname;
    private int age;
    private int trickLevel; // (целое число от 0 до 100)
    private HashSet<String> habits = new HashSet<>();
    Species species = Species.valueOf(this.getClass().getSimpleName().toUpperCase());

    public abstract void setSpecies(Species species);

    public String getNickname() {
        return nickname;
    }

    public Species getSpecies() {
        return species;
    }

    public int getAge() {
        return age;
    }

    public int getTrickLevel() {
        return trickLevel;
    }

    public HashSet<String> getHabits() {
        return habits;
    }

    void eat() {
        System.out.println("Я кушаю!");
    }

    abstract void respond();



    public Pet(Species species, String nickname, int age, int trickLevel, String[] habits) {
        this.nickname = nickname;
    }

    public Pet(String nickname, int age, int trickLevel, HashSet<String> habits) {
        this.nickname = nickname;
        this.age = age;
        if(trickLevel >= 0 && trickLevel <= 100) {
            this.trickLevel = trickLevel;
        }
        this.habits = habits;
    }

//    public void setSpecies(Species species) {
//        this.species = species;
//    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setTrickLevel(int trickLevel) {
        this.trickLevel = trickLevel;
    }

    public void setHabits(HashSet<String> habits) {
        this.habits = habits;
    }

    public Pet() {
    }

    @Override
    public String toString() {
        return "" + species +"{" +
                ", nickname='" + nickname + '\'' +
                ", age=" + age +
                ", trickLevel=" + trickLevel +
                ", habits=" + habits.toArray(new String[habits.size()]) +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pet pet = (Pet) o;
        return age == pet.age &&
                trickLevel == pet.trickLevel &&
                Objects.equals(species, pet.species) &&
                Objects.equals(nickname, pet.nickname) &&
                habits.equals(pet.habits);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(species, nickname, age, trickLevel);
        result = 31 * result + habits.hashCode();
        return result;
    }

    protected void finalize() {
        this.toString();
    }
}

package hw08;

import java.util.HashMap;

public class hw8 {


    public static void main(String[] args) {

        Human realDonaldTrump = new Human("Donald", "Trump", 74);
        Human melaniaKnauss = new Human("Melania", "Knauss", 50);
        Human ivankaTrump = new Human("Ivanka", "Trump", 39);
        Human fakeIvankaTrump = new Human("Fake Ivanka", "Trump", 39);

        HashMap<DayOfWeek, String> scheldure = new HashMap<>();

        scheldure.put(DayOfWeek.MONDAY, "Post new tweet");
        scheldure.put(DayOfWeek.TUESDAY, "Post another tweet");

        realDonaldTrump.setSchedule( scheldure );

        Pet unexistingDog = new Dog("I don't have time for him");

        Family trumpFamily = new Family(melaniaKnauss, realDonaldTrump);
        trumpFamily.addChild(ivankaTrump);
        trumpFamily.addChild(fakeIvankaTrump);
        trumpFamily.deleteChild(1);
//
        trumpFamily.setPet(unexistingDog);

        System.out.println(trumpFamily.toString());


        FamilyController familyController = new FamilyController();
    }
}

package hw02;

import java.util.Random;
import java.util.Arrays;
import java.util.Scanner;

public class hw2 {

    static int enterShot(String message) {
        System.out.println(message);
        Scanner scanner = new Scanner(System.in);
        return scanner.nextInt();
    }

    static void playerShot(int[][] playerTable, int[] enemyPosition) {
        int y = -1;
        int x = -1;
        int[][] gameTable = playerTable;
//                new int[playerTable.length][playerTable[1].length];

//        for (int i = 0; i < playerTable.length; i++) {
//            gameTable[i] = playerTable[i];
//        }

        while (y != enemyPosition[0] && x != enemyPosition[1] ) {
            y = enterShot("Введи горизонтальное положение выстрела от 1 до 5");
            x = enterShot("Введи вертикальное положение выстрела от 1 до 5");

            gameTable[x-1][y-1] = 2;
            if (y != enemyPosition[0] && x != enemyPosition[1]) {
                drawTable(gameTable);
            }
        }
        gameTable[x][y] = 3;
        drawTable(gameTable);
    }

    static void drawTable(int[][] arr) {
        System.out.println("0|1|2|3|4|5|");
        for (int i = 0; i < arr.length; i++) {
            System.out.print(i + 1 + "|");
            for (int j = 0; j < arr[i].length; j++) {

                if (arr[i][j] == 0) { // добавить на проверку 1
               //    if (arr[i][j] == 0 || arr[i][j] == 1) {
                    System.out.print("-"); //  Пустая ячейка
                } else if (arr[i][j] == 2){
                    System.out.print("*"); // Ячейка по которому был произведен выстрел
                } else if (arr[i][j] == 3) {
                    System.out.print("x"); // Ячейка по которой попали
                } else if (arr[i][j] == 1) {
                    System.out.print("O"); // Показывает где находится ответ. Удалить этот вариант
                }

                System.out.print("|");
            }
            System.out.println("");
        }
    }

    static int[][] generateTable(int[] enemyPosition) {
        int[][] gameTable = new int[5][5];

        gameTable[enemyPosition[1]][enemyPosition[0]] = 1;
        return gameTable;
    }

    static int[] generateRndNumber() {
        Random rnd = new Random();
        int y = rnd.nextInt(4);
        int x = rnd.nextInt(4);
        int[] numbers = {y, x};
        return numbers;
    }

    public static void main(String[] args) {

        int[] enemyPosition = generateRndNumber();
        int[][] gameTable = generateTable(enemyPosition);

        drawTable(gameTable);

        playerShot(gameTable, enemyPosition);

        System.out.println("You have won!");

    }
}

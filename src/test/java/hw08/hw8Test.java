package hw08;

import hw08.*;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class hw8Test {

    Human realDonaldTrump = new Human("Donald", "Trump", 74);
    Human melaniaKnauss = new Human("Melania", "Knauss", 50);
    Human ivankaTrump = new Human("Ivanka", "Trump", 39);
    Human fakeIvankaTrump = new Human("Fake Ivanka", "Trump", 39);

    Pet unexistingDog = new Dog("I don't have time for him");

    Family trumpFamily = new Family(melaniaKnauss, realDonaldTrump);

    FamilyController familyController = new FamilyController();



    @Test
    public void testToString() {
        assertEquals("Human{name='Donald', surname='Trump', year=74, iq=0, schedule={}}" ,realDonaldTrump.toString());
        assertEquals("Dog{nickname='I don't have time for him', age=0, trickLevel=0, habits=[]}", unexistingDog.toString());
    }

    @Test
    public void testDeleteChildObject() {
        trumpFamily.addChild(ivankaTrump);
        trumpFamily.deleteChild(ivankaTrump);
        assertEquals(2, trumpFamily.countFamily());
    }

    @Test
    public void testDeleteChildObjectFakeDelete() {
        trumpFamily.addChild(ivankaTrump);
        trumpFamily.deleteChild(fakeIvankaTrump);
        assertEquals(3, trumpFamily.countFamily());
    }

    @Test
    public void testDeleteChildIndex() {
        trumpFamily.addChild(ivankaTrump);
        trumpFamily.deleteChild(0);
        assertEquals(2, trumpFamily.countFamily());
    }

    @Test
    public void testCountFamily() {
        trumpFamily.addChild(ivankaTrump);
        assertEquals(3, trumpFamily.countFamily());
    }

    @Test
    public void testAddChildFamily() {
        trumpFamily.addChild(ivankaTrump);
        assertEquals(3, trumpFamily.countFamily());
    }

    @Test
    public void testgetAllFamilies() {
        familyController.createNewFamily(realDonaldTrump, melaniaKnauss);
        assertEquals("[Family{mother=Human{name='Melania', surname='Knauss', year=50, iq=0, schedule={}}, father=Human{name='Donald', surname='Trump', year=74, iq=0, schedule={}}, children=[], pet=[]}]", familyController.getAllFamilies().toString());
    }

    @Test
    public void testGetFamilyById() {
        familyController.createNewFamily(realDonaldTrump, melaniaKnauss);
        assertEquals("Family{mother=Human{name='Melania', surname='Knauss', year=50, iq=0, schedule={}}, father=Human{name='Donald', surname='Trump', year=74, iq=0, schedule={}}, children=[], pet=[]}", familyController.getFamilyById(0).toString());
    }

    @Test
    public void testGetFamiliesBiggerThan() {
        familyController.createNewFamily(realDonaldTrump, melaniaKnauss);
        familyController.createNewFamily(realDonaldTrump, fakeIvankaTrump);
        familyController.getFamilyById(1).addChild(ivankaTrump);
        assertEquals("[Family{mother=Human{name='Fake Ivanka', surname='Trump', year=39, iq=0, schedule={}}, father=Human{name='Donald', surname='Trump', year=74, iq=0, schedule={}}, children=[Human{name='Ivanka', surname='Trump', year=39, iq=0, schedule={}}], pet=[]}]",familyController.getFamiliesBiggerThan(2).toString());
    }

    @Test
    public void testGetFamiliesLessThan() {
        familyController.createNewFamily(realDonaldTrump, melaniaKnauss);
        familyController.createNewFamily(realDonaldTrump, fakeIvankaTrump);
        familyController.getFamilyById(1).addChild(ivankaTrump);
        assertEquals("[Family{mother=Human{name='Melania', surname='Knauss', year=50, iq=0, schedule={}}, father=Human{name='Donald', surname='Trump', year=74, iq=0, schedule={}}, children=[], pet=[]}]",familyController.getFamiliesLessThan(3).toString());
    }

    @Test
    public void testCountFamiliesWithMemberNumber() {
        familyController.createNewFamily(realDonaldTrump, melaniaKnauss);
        familyController.createNewFamily(realDonaldTrump, fakeIvankaTrump);
        familyController.getFamilyById(1).addChild(ivankaTrump);
        assertEquals(1, familyController.countFamiliesWithMemberNumber(3));
    }

    @Test
    public void testDeleteFamilyByIndex() {
        familyController.createNewFamily(realDonaldTrump, melaniaKnauss);
        familyController.createNewFamily(realDonaldTrump, fakeIvankaTrump);
        familyController.getFamilyById(1).addChild(ivankaTrump);
        familyController.deleteFamilyByIndex(0);
        assertEquals( 1,familyController.count());
    }

//    @Test
//    public void testBornChild() {
//        familyController.createNewFamily(realDonaldTrump, melaniaKnauss);
//        familyController.bornChild(familyController.getFamilyById(0), "Ololo", "Ololosenka");
//    }

    @Test
    public void testAdoptChild() {
        familyController.createNewFamily(realDonaldTrump, melaniaKnauss);
        familyController.adoptChild(familyController.getFamilyById(0), ivankaTrump);
        assertEquals("Family{mother=Human{name='Melania', surname='Knauss', year=50, iq=0, schedule={}}, father=Human{name='Donald', surname='Trump', year=74, iq=0, schedule={}}, children=[Human{name='Ivanka', surname='Trump', year=39, iq=0, schedule={}}], pet=[]}", familyController.getFamilyById(0).toString());
    }

//    @Test
//      public void testDeleteAllChildrenOlderThen() {
//        familyController.createNewFamily(realDonaldTrump, melaniaKnauss);
//        familyController.adoptChild(familyController.getFamilyById(0), ivankaTrump);
//        familyController.deleteAllChildrenOlderThen(10);
//        assertEquals("", familyController.getFamilyById(0).toString());
//    }

    @Test
    public void testCount() {
        familyController.createNewFamily(realDonaldTrump, melaniaKnauss);
        assertEquals(1, familyController.count());
    }

    @Test
    public void testGetPet() {
        familyController.createNewFamily(realDonaldTrump, melaniaKnauss);
        familyController.getFamilyById(0).setPet(unexistingDog);
        assertEquals("[Dog{nickname='I don't have time for him', age=0, trickLevel=0, habits=[]}]", familyController.getPets(0).toString());
    }
}
